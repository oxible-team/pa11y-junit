FROM node:13.10

LABEL maintainer="veselin.genchev@oxible.com"

RUN apt-get update && apt-get upgrade -y \
	&& apt-get install -y \
		libatk-bridge2.0-0 \
		libx11-xcb1 \
		libnss3 \
		libxss1 \
		libasound2 \
		libgtk-3-0 \
	&& apt-get update \
	&& rm -rf /var/lib/apt/lists/*

RUN yarn global add pa11y pa11y-reporter-junit

ENTRYPOINT ["/bin/bash"]
